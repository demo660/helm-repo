# letsencrypt-issuer

A Helm chart for Kubernetes

**Homepage:** <https://cert-manager.io/docs/configuration/>

## Maintainers

| Name | Email | Url |
| ---- | ------ | --- |
| hagzag | hagzag@tikalk.dev |  |

## Values

| Key | Type | Default | Description |
|-----|------|---------|-------------|
| email | string | `"john-doe@example.com"` |  |
| issuers[0].kind | string | `"ClusterIssuer"` |  |
| issuers[0].name | string | `"letsencrypt-staging"` |  |
| issuers[0].url | string | `"https://acme-staging-v02.api.letsencrypt.org/directory"` |  |
| issuers[0].version | string | `"cert-manager.io/v1"` |  |
| issuers[1].kind | string | `"ClusterIssuer"` |  |
| issuers[1].name | string | `"letsencrypt-prod"` |  |
| issuers[1].url | string | `"https://acme-v02.api.letsencrypt.org/directory"` |  |
| issuers[1].version | string | `"cert-manager.io/v1"` |  |

